"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.shortcuts import HttpResponse, render_to_response
from django.urls import path
from django.views import View
from corecluster.utils.exception import CoreException
from corecluster.utils.context import Context
from functools import wraps
from corenetwork.utils.logger import log
from simplejson import encoder, decoder
import corecluster.utils.validation as v


def validate(**validators):
    def _validate(f):
        @wraps(f)
        def call(context, **kwargs):
            for param in validators:
                if hasattr(validators, 'none') and validators.none is True and param not in kwargs:
                    continue

                if param not in kwargs:
                    return {'status': 'error', 'data': 'missing_parameter_%s' % param}

                try:
                    validators[param](param, kwargs[param])
                except CoreException as e:
                    return {'status': 'error', 'data': str(e)}

            try:
                r = f(context, **kwargs)
                return {'status': 'ok', 'data': r}
            except CoreException as e:
                return {'status': 'error', 'data': str(e)}
            except Exception as e:
                return {'status': 'fatal_error', 'data': str(e)}

        return call
    return _validate


def render_as_ui(f):
    '''
    Renders function result as HTML page with given in parameter template. Should be placed over the validate decorator
    to properly catch all errors.
    '''

    @wraps(f)
    def call(self, *args, **kwargs):
        if '__render_as_json' in kwargs:
            del kwargs['__render_as_json']
            return render_to_response(self.model.__name__ + '/' + f.__name__, self.f(self, *args, **kwargs))
        elif '__render_as_html' in kwargs:
            del kwargs['__render_as_html']

            enc = encoder.JSONEncoder()
            return enc.encode(self.model.__name__.lower() + '/' + f.__name__, self.f(self, *args, **kwargs))
        else:
            return f.__doc__

    return call


class ApiInterface(View):
    model = None
    auth = None
    log = None
    expose_methods = ['get_by_id', 'get_list', 'edit', 'describe']

    @classmethod
    def return_error(cls, request, error_message, error_status, log_message, exception=None, doc=None):
        enc = encoder.JSONEncoder()

        log(msg=log_message, loglevel='error', tags=['call'], exception=exception)

        if request.META.get('HTTP_ACCEPT', 'text/json') in ('text/json', 'application/json'):
            return HttpResponse(enc.encode({'status': 'error', 'data': error_message}), status=error_status)
        else:
            return render_to_response('error.html', {'error': error_message, 'doc': doc}, status=error_status)

    @classmethod
    def auth(cls, api_module, request, data):
        # Do auth
        user = None
        node = None
        vm = None
        try:
            if api_module.auth == 'token':
                user, data = cls.auth_module.auth_token(data, request.get_full_path())
            elif api_module.auth == 'password':
                user, data = cls.auth_module.auth_password(data, request.get_full_path())
            elif api_module.auth == 'guest':
                pass
            elif api_module.auth == 'node':
                node, data = cls.auth_module.auth_node(data, request.META['REMOTE_ADDR'])
            else:
                raise CoreException('unsupported_auth_method')
        except CoreException as e:
            log(msg='Authentication failed', exception=e, loglevel='warning', tags=['call'])
            return cls.return_error(request, 'Authentication failed', 403, 'Failed to authenticate user', exception=e)
        except Exception as e:
            log(msg='Authentication failed', exception=e, loglevel='error', tags=['call'])
            return cls.return_error(request, 'Authentication failed', 403, 'Failed to authenticate user', exception=e)

        context = Context(user=user, node=node, vm=vm, remote_ip=request.META['REMOTE_ADDR'], request=request)
        return context

    @classmethod
    def process_request(cls, request, method):
        enc = encoder.JSONEncoder()
        dec = decoder.JSONDecoder()
        api_module = cls()

        # Check if function is in exposed methods
        if method not in cls.expose_methods:
            return cls.return_error(request, 'Function not found', 404, 'Method %s/%s not found' % (cls.__name__, method))

        if not hasattr(api_module, method):
            return cls.return_error(request, 'Function not found', 404, 'Missing API method %s in %s' % (method, cls.__name__))

        f = getattr(api_module, method)

        if request.method == 'OPTIONS':
            resp = HttpResponse()
            resp['Allow'] = 'POST, OPTIONS'
            resp['Accept'] = 'application/json'
            resp['Access-Control-Allow-Headers'] = 'Content-Type'
            resp['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
            resp['Access-Control-Allow-Origin'] = '*'
            return resp

        # Decode parameters
        try:
            data = dec.decode(request.POST)
        except Exception as e:
            return cls.return_error(request, 'Failed to parse parameters', 400, 'Failed to parse parameters: %s' % str(e), doc=f.__doc__)

        context = cls.auth(api_module, request, data)
        # Here we are authenticated

        if method == 'POST' and request.META.get('HTTP_ACCEPT', 'text/json') in ('text/json', 'application/json'):
            data['__render_as_json'] = True
            try:
                return HttpResponse(enc.encode(f(api_module, context, **data)))
            except Exception as e:
                cls.return_error(request, 'Internal server error', 500, 'Call failed', e)
        elif method == 'POST':
            data['__render_as_html'] = True
            try:
                return f(api_module, context, **data)
            except Exception as e:
                cls.return_error(request, 'Internal server error', 500, 'Call failed', e)

    @classmethod
    def as_view(cls):
        return cls.process_request

    @render_as_ui
    @validate(exclude=v.is_list(none=True, empty=True),
              order_by=v.is_list(none=True, empty=True))
    def get_list(self, context, exclude=None, order_by=None):
        #TODO: check if exclude and order_by columns are present in model
        items = self.model.get_list(context.user_id, exclude=exclude, order_by=order_by)
        return [i.to_dict for i in items]

    @render_as_ui
    @validate(id=v.is_id())
    def get_by_id(self, context, id):
        item = self.model.get(context.user_id, id)
        return item.to_dict

    @render_as_ui
    @validate(id=v.is_id())
    def edit(self, context, id, **kwargs):
        item = self.model.get(context.user_id, id)
        item.edit(context, **kwargs)

    @render_as_ui
    def describe(self, context):
        return self.model.describe_model()