"""
Copyright (c) 2014 Maciej Nabozny

This file is part of CloudOver project.

CloudOver is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from corecluster.views.admin.storage import StorageAdmin
from corecluster.views.admin.user import UserAdmin
from corecluster.views.admin.role import RoleAdmin
from corecluster.views.admin.group import GroupAdmin
from corecluster.views.admin.image import ImageAdmin
from corecluster.views.admin.template import TemplateAdmin
from corecluster.views.admin.node import NodeAdmin
from corecluster.views.admin.subnet import Subnet
from corecluster.views.admin.network_pool import NetworkPool
from corecluster.views.admin.agent import AgentAdmin
from corecluster.views.admin.vm import VMAdmin
from corecluster.views.admin.permission import PermissionAdmin
from corecluster.views.admin.message import MessageAdmin
