"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import urllib
from corecluster.utils.exception import CoreException
from corecluster.models.core.image import Image
from corecluster.models.core.vm import VM
from corecluster.cache.task import Task
from corecluster.cache.data_chunk import DataChunk
from corenetwork.utils import config
from corenetwork.utils.logger import log
import corecluster.utils.validation as v
from corecluster.utils.api_interface import ApiInterface, validate, render_as_ui


class Api(ApiInterface):
    model = Image
    auth = 'token'
    log = 'all'
    expose_methods = [
        'get_list',
        'get_by_id',
        'describe',
        'edit',
        'create',
        'upload_url',
        'upload_data',
        'delete',
        'get_disk_controllers',
        'get_video_devices',
        'get_network_devices',
        'get_image_formats',
        'get_image_types',
        'attach',
        'detach',
    ]

    @render_as_ui
    @validate(name=v.is_string(),
              description=v.is_string(),
              size=v.is_integer(),
              image_type=v.is_string(),
              disk_controller=v.is_string(none=True),
              access=v.is_string(none=True),
              format=v.is_string(none=True))
    def create(self, context, name, description, size, image_type, disk_controller=config.get('hardware', 'default_disk_controller'), access='private', format=config.get('hardware', 'default_image_format')):
        """ Create new image
        :param name: image name
        :param description: image description
        :param size: image size in bytes
        :param image_type: type of image: transient, permanent, cd
        :param disk_controller: disk controller for this image (list available by get_disk_controllers)
        :param access: defines if image should be available only for owner, group or public
        """
        context.user.check_storage_quota(size)

        image = Image.create(context.user, name, description, long(size), image_type, disk_controller, access, format)
        image.save()

        task = Task(user=context.user)
        task.type = 'image'
        task.state = 'not active'
        task.action = 'create'
        task.append_to([image, image.storage])

        return image.to_dict

    @render_as_ui
    @validate(id=v.is_id(), url=v.is_string())
    def upload_url(self, context, id, url):
        """
        Upload data into given image from URL
        :param id: Id of image
        :param url: address with image's data
        :return: dictionary with image info
        """
        try:
            size = int(urllib.urlopen(url).info().getheaders('Content-Length')[0])
        except Exception as e:
            log(exception=e, msg='Cannot download image', context=context)
            raise CoreException('image_download')

        image = Image.get(context.user_id, id)

        task = Task(user=context.user)
        task.type = 'image'
        task.state = 'not active'
        task.action = 'upload_url'
        task.set_all_props({'offset': 0,
                            'size': size,
                            'url': url})
        task.append_to([image, image.storage])

        return image.to_dict

    @render_as_ui
    @validate(id=v.is_id(), offset=v.is_integer(), data=v.is_string())
    def upload_data(self, context, id, offset, data):
        """
        Upload chunk of data into image
        :param id: Id of the image
        :param offset: Data offset
        :param data: Data to be uploaded, encoded with base64
        :return: Dictionary with image
        """
        image = Image.get(context.user_id, id)

        if len(data) > config.get('core', 'MAX_UPLOAD_CHUNK_SIZE', 1024*1024*10):
            raise CoreException('image_data_too_large')

        chunk = DataChunk()
        chunk.data = data
        chunk.offset = offset
        chunk.image_id = image.id
        chunk.type = 'upload'
        chunk.save()

        task = Task(user=context.user)
        task.type = 'image'
        task.state = 'not active'
        task.action = 'upload_data'
        task.set_all_props({'offset': offset,
                            'size': len(data),
                            'chunk_id': chunk.cache_key()})

        task.append_to([image])

        return image.to_dict

    @render_as_ui
    @validate(id=v.is_id())
    def delete(self, context, id):
        """ Delete image """
        image = Image.get(context.user_id, id)

        if image.user_id != context.user_id:
            raise CoreException('not_owner')

        if not image.in_states(['ok', 'failed']) and config.get('core', 'CHECK_STATES'):
            raise CoreException('image_wrong_state')

        if image.attached_to != None and config.get('core', 'CHECK_STATES', False):
            raise CoreException('image_attached')

        task = Task(user=context.user)
        task.type = 'image'
        task.state = 'not active'
        task.action = 'delete'
        task.append_to([image])

    @render_as_ui
    def get_disk_controllers(self, context):
        """ Get list of disk controllers. Disk controller of image is used, when
        attaching disk to virtual machine.
        """
        return config.get('hardware', 'disk_controllers')

    @render_as_ui
    def get_video_devices(self, context):
        """ Get list of video devices. Video device (driver) is used, when
        creating new virtual machine with given image as base_image
        """
        return config.get('hardware', 'video_devices')

    @render_as_ui
    def get_network_devices(self, context):
        """ Get list of network devices. Network device (driver) is used, when
        creating new virtual machine with given image as base_image
        """
        return config.get('hardware', 'network_devices')

    @render_as_ui
    def get_image_formats(self, context):
        """ Get list of network devices. Network device (driver) is used, when
        creating new virtual machine with given image as base_image
        """
        return config.get('hardware', 'image_formats')

    @render_as_ui
    def get_image_types(self, context):
        """ Return all types of images """
        return Image.image_types

    @render_as_ui
    @validate(id=v.is_id(), vm_id=v.is_id(), device=v.is_integer(none=True))
    def attach(self, context, id, vm_id, device=None):
        """
        Attach image to VM as external disk
        :param id: An image to be attached
        :param vm_id: Instance, which should get new storage device
        :param device: device index (sda=1, sdb=2, ...), optional
        """
        image = Image.get(context.user_id, id)
        vm = VM.get(context.user_id, vm_id)

        if not vm.in_state('stopped') and config.get('core', 'CHECK_STATES', False):
            raise CoreException('vm_not_stopped')

        task = Task(user=context.user)
        task.type = 'image'
        task.state = 'not active'
        task.action = 'attach'
        if device is not None:
            task.set_prop('device', device)
        task.append_to([image, vm])

    @render_as_ui
    @validate(id=v.is_id(), vm_id=v.is_id())
    def detach(self, context, id, vm_id):
        """ Detach image from VM """
        image = Image.get(context.user_id, id)
        vm = VM.get(context.user_id, vm_id)

        if not vm.in_state('stopped') and config.get('core', 'CHECK_STATES', False):
            raise CoreException('vm_not_stopped')

        task = Task(user=context.user)
        task.type = 'image'
        task.state = 'not active'
        task.action = 'detach'
        task.append_to([vm, image])
