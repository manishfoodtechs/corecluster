"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import hashlib
import datetime
import random

from corecluster.models.core.token import Token
import corecluster.utils.validation as v
from corecluster.utils.api_interface import ApiInterface, validate, render_as_ui


class Api(ApiInterface):
    model = Token
    auth = 'password'
    log = 'all'
    expose_methods = [
        'get_list',
        'get_by_id',
        'describe',
        'edit',
        'create',
        'delete',
    ]

    failed_seeds = {}

    @validate(name=v.is_string(none=True), token_valid_to=v.is_datetime(none=True))
    @render_as_ui
    def create(self, context, name='', token_valid_to=None):
        """
        Create new token
        :param name: Name of token
        :param token_valid_to: How long token should be valid (datetime string)
        :return: Dictionary with token's info
        """
        token = Token()
        token.user_id = context.user_id
        token.token = hashlib.sha1(str(random.random())).hexdigest()
        token.creation_date = datetime.datetime.now()
        token.name = name
        token.ignore_permissions = True
        if token_valid_to is not None:
            token.valid_to = token_valid_to
        else:
            token.valid_to = datetime.datetime.now()+datetime.timedelta(weeks=1)

        token.save()

        return token.to_dict

    @validate(id=v.is_id())
    @render_as_ui
    def delete(self, context, id):
        """
        Delete token
        :param id: Token id to be deleted
        """
        token = Token.get(context.user_id, id)
        token.remove(context)
