#!/usr/bin/python3
#-*- coding: utf-8 -*-
"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import signal
import django
import time
import sys
import os
import requests
import urllib

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "corecluster.settings")
django.setup()

from corenetwork.driver_interface import DriverInterface
from corenetwork.utils.logger import log
from corenetwork.utils import coremetric

i_am_running = True

drivers = DriverInterface.get_all_drivers()


def handler(signum, frame):
    global i_am_running

    print("Stopping agents...")
    for driver in drivers:
        driver.shutdown_core()
    i_am_running = False


from corecluster.models.core.cluster_id import ClusterID
from corenetwork.utils import config
from corecluster import version

if __name__ == "__main__":
    try:
        if sys.argv[1] == 'configure':
            coremetric.log('coremetric/register-management/', {'cluster_id': ClusterID.obtain_id(),
                                                               'management_id': config.get('core', 'INSTALLATION_ID')})

            for driver in drivers:
                driver.configure_core()
        elif sys.argv[1] == 'agent' and sys.argv[2] == 'start':
            coremetric.log('coremetric/update-management/', {'id': config.get('core', 'INSTALLATION_ID'),
                                                             'version': version.version,
                                                             'apps': urllib.parse.quote_plus(str(config.get('core', 'APPS'))),
                                                             'state': 'starting'})

            for driver in drivers:
                driver.startup_core()

            signal.signal(signal.SIGTERM, handler)
            signal.signal(signal.SIGINT, handler)

            coremetric.log('coremetric/update-management/', {'id': config.get('core', 'INSTALLATION_ID'),
                                                             'version': version.version,
                                                             'state': 'agents-running'})
            print("Ok")

            while i_am_running:
                time.sleep(1)

            coremetric.log('coremetric/update-management/', {'id': config.get('core', 'INSTALLATION_ID'),
                                                             'version': version.version,
                                                             'state': 'stopped'})

        else:
            DriverInterface.get_core_driver().cli_handle(sys.argv)

    except Exception as e:
        print("Error occurred: " + str(e))
        log(msg="failed to start action %s" % sys.argv[1], function="cc-manage", exception=e)
        coremetric.log('coremetric/update-management/', {'id': config.get('core', 'INSTALLATION_ID'),
                                                         'version': version.version,
                                                         'apps': urllib.parse.quote_plus(str(config.get('core', 'APPS'))),
                                                         'state': 'failed'})
