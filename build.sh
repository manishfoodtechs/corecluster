#!/bin/bash -x

BRANCH=$1
VERSION=`date +'%y.%m.%d%H%M'`
FLAVORS=( deb rpm tar sh )
PACKAGES=( corecluster corecluster-algorithm-id-uuid corecluster-algorithm-node-fillup corecluster-algorithm-node-median corecluster-algorithm-storage-default corecluster-auth-db corecluster-storage-libvirt corecluster-storage-sheepdog corecluster-storage-ssh coredhcp coremetry corenetwork corenode coretalk corevpn )
BUILDDIR=/data/build/

DEB_VERSIONS=( stretch xenial yakkety zasty artful )

aptly repo create cloudover

for NAME in "${PACKAGES[@]}" ; do
	rm -rf $BUILDDIR/build-repo-$NAME
	git clone https://github.com/cloudover/$NAME $BUILDDIR/build-repo-$NAME
	rm -rf $BUILDDIR/build-$NAME
	mkdir $BUILDDIR/build-$NAME
	mkdir -p $BUILDDIR/packages/$VERSION
	mkdir -p $BUILDDIR/build-$NAME/etc/$NAME

	t=`pwd`
	cd $BUILDDIR/build-repo-$NAME ; make VERSION=$VERSION DESTDIR=$BUILDDIR/build-$NAME
	cd $t

	for FLAVOR in "${FLAVORS[@]}" ; do
		mkdir -p $BUILDDIR/packages/$VERSION/$FLAVOR
		DEPS=`cat $BUILDDIR/build-repo-$NAME/deps.$FLAVOR | awk '{ print " -d " $0 }'`

		fpm -s dir $DEPS \
			-a all \
			-t $FLAVOR \
			-n $NAME \
			-v $VERSION \
			-m contact@cloudover.io \
			--vendor "cloudover.io ltd." \
			--license "Dual, AGPL3" \
			--description "CoreCluster cloud" \
			--after-install $BUILDDIR/build-repo-$NAME/$NAME.install.sh \
			--config-files /etc/$NAME \
			-C $BUILDDIR/build-$NAME .
		mv $NAME*.$FLAVOR $BUILDDIR/packages/$VERSION/$FLAVOR/$NAME-$VERSION.$FLAVOR
		
		if [ "x$FLAVOR" == "xdeb" ] ; then
			aptly repo add cloudover $BUILDDIR/packages/$VERSION/$FLAVOR/$NAME-$VERSION.$FLAVOR
		fi
	done

	cd $BUILDDIR/packages/$VERSION/deb/
	dpkg-scanpackages . | gzip > $BUILDDIR/packages/$VERSION/deb/Packages.gz
	cd $pwd
done

aptly snapshot create $VERSION from repo cloudover
for DISTRO in "${DEB_VERSIONS[@]}" ; do
	aptly publish snapshot -distribution=$DISTRO -architectures=all $VERSION
	aptly publish switch $DISTRO -architectures=all $VERSION
done

if ! [ "x$1" == "x" ] ; then
	rsync -av ~/.aptly/public/ $1
fi
